package com.example.root.myapplication.Controller;

import com.example.root.myapplication.Model.User;
                                                                                                                                                                                                    import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiService {

    public static final String BASE_URL = "http://www.wishare.com.br/code/model/method.json";

    @GET("basic")
    Call<User> login(@Query("model") String model,@Query("method")String method, @Header("Authorization") String authHeader);
}

